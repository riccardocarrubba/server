const express = require('express');
const cors = require('cors')
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const db = require("./models");

//Route
const indexRouter = require('./routes');
const authRouter = require('./routes/auth.routes');
const usersRouter = require('./routes/users.routes');
const structuresRouter = require('./routes/structures.routes');
const customersRouter = require('./routes/customers.routes');
const roomsRouter = require('./routes/rooms.routes');
const booksRouter = require('./routes/books.routes');

const app = express();
app.use(cors());


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.options('*', cors()) // da includere prima delle altre routes

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/structures', structuresRouter);
app.use('/auth', authRouter);
app.use('/customers', customersRouter);
app.use('/rooms', roomsRouter);
app.use('/books', booksRouter);


db.sequelize.sync();


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json(err.message);
  res.render('error');
});

module.exports = app;
