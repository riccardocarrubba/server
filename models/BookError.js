"use strict";
module.exports = class BookError extends Error {
  constructor(message) {
    super(message);
    this.name = "BookError";
  }
};
