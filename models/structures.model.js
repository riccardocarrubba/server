module.exports = (sequelize, Sequelize) => {
    return sequelize.define("struttura", {
            id: {
                type: Sequelize.BIGINT,
                autoIncrement: true,
                primaryKey: true
            },
            nome: {
                type: Sequelize.STRING,
            },
            descrizione: {
                type: Sequelize.STRING
            },
            citta: {
                type: Sequelize.STRING,
            },
            indirizzo: {
                type: Sequelize.STRING,
            },
            cap: {
                type: Sequelize.STRING,
            },
            tipologia: {
                type: Sequelize.INTEGER,
            },
            tassa: {
                type: Sequelize.INTEGER,
            },
            prezzo: {
                type: Sequelize.INTEGER,
            },
            telefono: {
                type: Sequelize.STRING,
            },
            wifi: {
                type: Sequelize.BOOLEAN,
            },
            parcheggio: {
                type: Sequelize.BOOLEAN,
            },
            ascensore_disabili: {
                type: Sequelize.BOOLEAN,
            },
            posti_letto: {
                type: Sequelize.INTEGER,
            },
            utente_id: {
                type: Sequelize.BIGINT,
                references: {
                    model: 'utente',
                    key: 'id',
                }
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        });
};
