module.exports = (sequelize, Sequelize) => {
    return sequelize.define("utente", {
            id: {
                type: Sequelize.BIGINT,
                primaryKey: true,
                autoIncrement: true
            },
            mail: {
                type: Sequelize.STRING,
                unique: true
            },
            nome: {
                type: Sequelize.STRING,
            },
            cognome: {
                type: Sequelize.STRING,
            },
            data_nascita: {
                type: Sequelize.DATE,
            },
            password: {
                type: Sequelize.STRING,
            },
            citta: {
                type: Sequelize.BOOLEAN,
            },
            telefono: {
                type: Sequelize.STRING,
            },
            tipologia: {
                type: Sequelize.BOOLEAN
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        });
};
