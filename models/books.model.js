module.exports = (sequelize, Sequelize) => {
    return sequelize.define("prenotazione", {
            id: {
                type: Sequelize.BIGINT,
                primaryKey: true,
                autoIncrement: true
            },
            prezzo: {
                type: Sequelize.INTEGER,
            },
            tipologia_ospite: {
                type: Sequelize.STRING,
            },
            tassa: {
                type: Sequelize.BOOLEAN,
            },
            check_in: {
                type: Sequelize.DATE,
            },
            check_out: {
                type: Sequelize.DATE,
            },
            attesa: {
                type: Sequelize.BOOLEAN,
            },
            creata_il: {
                type: 'TIMESTAMP',
                defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
                allowNull: false
            },
            nome_utente: {
                type: Sequelize.STRING,
            },
            cognome_utente: {
                type: Sequelize.STRING,
            },
            nome_camera: {
                type: Sequelize.STRING,
            },
            nome_struttura: {
                type: Sequelize.STRING,
            },
            id_proprietario: {
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
                references: {
                    model: "utente",
                    key: "id",
                }
            },
            id_camera: {
                type: Sequelize.BIGINT,
            },
            id_struttura: {
                type: Sequelize.BIGINT,
            },
            id_cliente: {
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
                references: {
                    model: "utente",
                    key: "id",
                }
            },
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};
