module.exports = (sequelize, Sequelize) => {
    return sequelize.define("camera", {
            id: {
                type: Sequelize.BIGINT,
                autoIncrement: true,
                primaryKey: true,
            },
            nome: {
                type: Sequelize.STRING,
            },
            descrizione: {
                type: Sequelize.STRING,
            },
            prezzo: {
                type: Sequelize.INTEGER,
            },
            posti_letto: {
                type: Sequelize.INTEGER,
            },
            area_fumatori: {
                type: Sequelize.BOOLEAN,
            },
            bagno_disabili: {
                type: Sequelize.BOOLEAN,
            },
            animali: {
                type: Sequelize.BOOLEAN,
            },
            path: {
                type: Sequelize.STRING,
            },
            id_struttura: {
                type: Sequelize.BIGINT,
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
                references: {
                    model: "struttura",
                    key: "id",
                }
            }
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );
};
