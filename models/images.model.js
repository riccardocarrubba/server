module.exports = (sequelize, Sequelize) => {
    return sequelize.define(
        "immagini_struttura",
        {
            id_struttura: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
                references: {
                    model: "struttura",
                    key: "id"
                }
            },
            path: {
                type: Sequelize.STRING,
                primaryKey: true,
            },
        },
        {
            freezeTableName: true,
            timestamps: false,
        }
    );

};
