const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth.controller')


router.post('/signin', authController.signin);
router.post('/refreshToken', authController.refreshToken);
router.post('/signup', authController.signup);
router.post('/signupOwner', authController.signupProprietario);
module.exports = router;
