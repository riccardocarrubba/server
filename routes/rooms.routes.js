const express = require("express");
const router = express.Router();
const roomsController = require("../controllers/rooms.controller");
const utilsController = require("../controllers/utils.controller");

// GET users listing.
router.get("/", async (req, res) => {
  res.send("respond with a resource");
});

router.post("/createRoom", utilsController.upload, roomsController.createRoom);
router.post("/getRoomsByStructure", roomsController.getRoomsByStructure);
router.put("/updateRooms", roomsController.updateRooms);
router.put("/updateImages",utilsController.upload, roomsController.updateImages);
router.post("/deleteRoomsById", roomsController.deleteRoomsById);
module.exports = router;
