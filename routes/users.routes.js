const express = require('express');
const router = express.Router();
const usersController = require('../controllers/users.controller')


router.get('/', (req, res) => {res.render('index', {title: 'Express'});});
router.put('/passModify', usersController.passModify);
router.put('/update', usersController.update);

module.exports = router;
