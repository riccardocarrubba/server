const express = require('express');
const router = express.Router();
const customersController = require('../controllers/customers.controller')
const utilsController = require('../controllers/utils.controller')

// GET users listing.
router.get('/', async (req, res) => {
    res.send('respond with a resource');
});

router.post('/sendThreeMonthReport', customersController.sendThreeMonthReport)
router.post('/sendDataToPolice', customersController.sendDataToPolice)


module.exports = router;
