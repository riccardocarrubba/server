const express = require('express');
const router = express.Router();
const structuresController = require('../controllers/structures.controller')


// GET users listing.
router.get('/', async (req, res) => {
    res.send('respond with a resource');

});

router.post('/createStructure', structuresController.creaStruttura)
router.post('/getStructuresByUser', structuresController.getStructuresByUser)
router.post('/deleteStructureById', structuresController.deleteStructureById)
router.post('/uploadImage', structuresController.uploadImage)
router.post("/search", structuresController.search);
router.post("/searchRoomsByStructure", structuresController.searchRoomsByStructure);
router.put('/updateStructures', structuresController.updateStructures);
router.post("/deleteImages", structuresController.deleteImages);
module.exports = router;
