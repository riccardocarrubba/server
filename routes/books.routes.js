const express = require('express');
const router = express.Router();
const booksController = require('../controllers/books.controller')

// GET users listing.
router.get('/getAllBooks', booksController.getAllBooks);

router.post('/allBooksCliente', booksController.allBooksCliente);

router.post('/getPendingBooksByUser', booksController.getPendingBooksByUser)
router.post('/getHistoryBooksByUser', booksController.getHistoryBooksByUser)
router.post('/deletePendingBooksById', booksController.deletePendingBooksById)
router.post('/acceptPendingBooks', booksController.acceptPendingBooks)
router.post('/createBook', booksController.createBook)
router.post('/verifyDate', booksController.verifyDate)
router.post("/verificaGuadagni", booksController.verificaGuadagni);
router.post("/checkCreditCard", booksController.checkCreditCard);


module.exports = router;
