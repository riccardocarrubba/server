const createError = require('http-errors');
const db = require("../models");
const Rooms = db.rooms;
const AuthError = require('../models/AuthError');
const utilsController = require('../controllers/utils.controller');
const BookError = require("../models/BookError");
const bookController = require("../controllers/books.controller");
const authController = require('../controllers/auth.controller')

exports.createRoom = async (req, res, next) => {
    let stanza = JSON.parse(req.body.anotherdata);
    const {nome, descrizione, prezzo, posti_letto, area_fumatori, bagno_disabili, animali, id_struttura,} = stanza;
    const files = req.files;
    try {
        await authController.checkToken(req);
        if (
            (id_struttura !== undefined) &&
            (nome !== undefined) &&
            (descrizione !== undefined) &&
            (prezzo !== undefined) &&
            (posti_letto !== undefined) &&
            (area_fumatori !== undefined) &&
            (bagno_disabili !== undefined) &&
            (animali !== undefined)
        ) {
            let room = await Rooms.create({
                nome: nome,
                descrizione: descrizione,
                prezzo: prezzo,
                posti_letto: posti_letto,
                area_fumatori: area_fumatori,
                bagno_disabili: bagno_disabili,
                animali: animali,
                id_struttura: id_struttura,
                path: files[0].filename,
            });
            res.send(room);
        } else {
            next(
                createError(422, " Uno o più campi non sono stati inseriti correttamente")
            );
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }
    }
};

exports.getRoomsByStructure = async (req, res, next) => {
    const {id} = req.body;
    if (id !== undefined) {
        try {
            let rooms = await Rooms.findAll({where: {id_struttura: id}})
            if (rooms !== null) {
                res.send(rooms)
            } else {
                next(createError(404, 'Non sono state trovate camere per questa struttura'));
            }
        } catch (e) {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }
    } else {
        next(createError(422, 'Camera non trovata'))
    }
}

exports.updateRooms = async (req, res, next) => {
    const {id, nome, descrizione, prezzo, posti_letto, area_fumatori, bagno_disabili, animali, path, id_struttura} = req.body;
    if ((id !== undefined) && (prezzo !== undefined) && (posti_letto !== undefined) && (area_fumatori !== undefined) && (bagno_disabili !== undefined) && (animali !== undefined) && nome && descrizione && path) {
        try {
            await authController.checkToken(req);
            let room = await Rooms.findOne({where: {id: id}});
            if (room !== null) {
                let room = await Rooms.update({
                    nome: nome,
                    descrizione: descrizione,
                    prezzo: prezzo,
                    posti_letto: posti_letto,
                    area_fumatori: area_fumatori,
                    bagno_disabili: bagno_disabili,
                    animali: animali,
                    id_struttura: id_struttura,
                    path: path
                }, {where: {id: id}});
                if (room[0] === 1) {
                    res.send(req.body);
                } else {
                    next(createError(400, 'Campi uguali a quelli precedenti'));
                }
            } else {
                next(createError(404, 'Camera non trovata'));
            }
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }

    } else {
        next(createError(422, " una proprietà fra quelle inserite non è corretta"))
    }

}

exports.updateImages = async (req, res, next) => {
    const files = req.files[0];
    const {id, nome, descrizione, prezzo, posti_letto, area_fumatori, bagno_disabili, animali, path, id_struttura} = JSON.parse(req.body.room);
    if ((id !== undefined) && (prezzo !== undefined) && (prezzo !== undefined) && (posti_letto !== undefined) && (area_fumatori !== undefined) && (bagno_disabili !== undefined) && (animali !== undefined)
        && (nome !== undefined) && (descrizione !== undefined) && (path !== undefined)) {
        try {
            let room = await Rooms.findOne({where: {id: id}});
            if (room !== null) {
                let roo = {
                    nome: nome,
                    descrizione: descrizione,
                    prezzo: prezzo,
                    posti_letto: posti_letto,
                    area_fumatori: area_fumatori,
                    bagno_disabili: bagno_disabili,
                    animali: animali,
                    id_struttura: id_struttura,
                    path: files.filename
                }
                let room = await Rooms.update(roo, {where: {id: id}});
                utilsController.removeImage(path)
                if (room[0] === 1) {
                    res.send(roo);
                } else {
                    next(createError(400, "Campi uguali a quelli precedenti"));
                }
            } else {
                next(createError(404, "Camera non trovata"));
            }
        } catch (e) {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }

    } else {
        next(createError(422, " una proprietà fra quelle inserite non è valida"))
    }

}

exports.deleteRoomsById = async (req, res, next) => {
    const {id_camera, path} = req.body.room;
    if (id_camera !== undefined) {
        try {
            await authController.checkToken(req);
            await bookController.checkBooksById(id_camera, true, null, new Date().toISOString().split('T')[0])
            let room = await Rooms.destroy({where: {id: id_camera}})
            if (room === 1) {
                utilsController.removeImage(path)
                res.send(true)
            } else {
                next(createError(400, "Camera non trovata"));
            }
        } catch (e) {
            if (e instanceof BookError) {
                next(createError(500, e));
            } else if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Camera non trovata"));
    }
}

