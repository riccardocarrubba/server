const createError = require('http-errors');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const jwtConfig = require('../config/jwt.config');
const db = require("../models");
const Users = db.users;
const Structures = db.structures;
const AuthError = require('../models/AuthError');

exports.signin = async (req, res, next) => {
    const {mail, password} = req.body;
    if (mail !== undefined && password !== undefined) {
        try {
            let user = await Users.findOne({where: {mail: mail}});
            if (user === null) {
                next(createError(404,'Utente non trovato' ));
            }
            let encpass = crypto.createHash("sha512").update(password).digest('hex');
            if (user.password !== encpass) {
                next(createError(401, 'Utente non autorizzato'));
            }
            const token = await jwt.sign({user}, jwtConfig.secret, {
                expiresIn: 86400 // 24 ore
            });

            user.setDataValue('token', token);
                res.send(user);
        } catch (e) {
            next(createError(401, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }
    } else {
        next(createError(422, "mail o password non sono definiti"))
    }
};

exports.refreshToken = async (req, res, next) => {
    try {
        const {user} = req.body;
        delete user.token
        const token = await jwt.sign({user}, jwtConfig.secret, {
            expiresIn: 86400 // 24 ore
        });
        user.token = token;
        res.send(user)
    }
    catch (e) {
        next(createError(500, e));
    }
}

exports.signup = async (req, res, next) => {
    const {nome, cognome, mail, data_nascita, password, citta, telefono, tipologia} = req.body;

    if (nome && cognome && mail && password && data_nascita && citta && telefono && (tipologia !== undefined)) {
        try {
            let encPass = crypto.createHash("sha512").update(password).digest('hex');
            let user = await Users.findOne({where: {mail: mail}});
            if (user === null) {
                user = await Users.create({
                    nome: nome,
                    cognome: cognome,
                    mail: mail,
                    password: encPass,
                    data_nascita: data_nascita,
                    citta: citta,
                    telefono: telefono,
                    tipologia: tipologia
                })
                const token = await jwt.sign({user}, jwtConfig.secret, {
                    expiresIn: 86400 // 24 ore
                });

                user.setDataValue('token', token);
                res.send(user)
            } else {
                next(createError(400, 'Email già utilizzata'));
            }
        } catch (e) {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }

    } else {
        next(createError(422, "una proprietà fra nome , cognome , mail , data_nascita , password , citta, telefono, tipologia non è stata definita"))
    }
}

exports.signupProprietario = async (req, res, next) => {
    const {utente} = req.body;
    const {struttura} = req.body;
    if (utente !== undefined && struttura !== undefined) {

        if (utente.nome && utente.cognome && utente.mail && utente.password && utente.data_nascita && utente.telefono && utente.telefono && (utente.tipologia !== null)) {
            if (struttura.nome && struttura.descrizione && struttura.citta && struttura.indirizzo && struttura.cap && struttura.telefono &&
                (struttura.tipologia !== null) && (struttura.wifi !== null) &&
                (struttura.parcheggio !== null) && (struttura.ascensore_disabili !== null)) {
                try {
                    let us = await Users.findOne({where: {mail: utente.mail}});
                    if (us === null) {
                        let encPass = crypto.createHash("sha512").update(utente.password).digest('hex');
                        us = await Users.create({
                            nome: utente.nome,
                            cognome: utente.cognome,
                            mail: utente.mail,
                            password: encPass,
                            data_nascita: utente.data_nascita,
                            citta: utente.citta,
                            telefono: utente.telefono,
                            tipologia: utente.tipologia
                        })
                        let st = await Structures.create({
                            nome: struttura.nome,
                            descrizione: struttura.descrizione,
                            utente_id: us.id,
                            citta: struttura.citta,
                            indirizzo: struttura.indirizzo,
                            cap: struttura.cap,
                            telefono: struttura.telefono,
                            wifi: struttura.wifi,
                            parcheggio: struttura.parcheggio,
                            tipologia: struttura.tipologia,
                            ascensore_disabili: struttura.ascensore_disabili
                        })
                        const token = await jwt.sign({us}, jwtConfig.secret, {
                            expiresIn: 86400 // 24 ore
                        });
                        us.setDataValue('token', token);

                        res.send({'utente': us, 'struttura': st})
                    } else {
                        next(createError(400, 'Email utente già utilizzata'));
                    }
                } catch (e) {
                    next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
                }
            } else {
                next(createError(422, '  una proprietà fra utente_id, nome, descrizione, citta , indirizzo, cap , tipologia, telefono, wifi, parcheggio, ascensore_disabili di struttura  '))
            }
        } else {
            next(createError(422, "una proprietà fra nome , cognome , mail , data_nascita , password , citta, telefono, tipologia di utente non è stata definita"))
        }
    } else {
        next(createError(422, " una proprietà fra user or structure not defined"))
    }

}

exports.checkToken = (req) => {
    return new Promise((resolve, reject) => {
        const authHeader = req.headers.authorization;
        if (authHeader === undefined) {
            reject(new AuthError("authHeader is not defined"))
        }
        const token = authHeader.split(' ')[1];
        jwt.verify(token, jwtConfig.secret, function(err, decoded) {
            if (err){
                reject(new AuthError("The jwt is not valid"))
            }
            resolve(decoded);
        });
    })
}


