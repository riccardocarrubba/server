const createError = require('http-errors');
const crypto = require('crypto');
const db = require("../models");
const Users = db.users;
const AuthError = require("../models/AuthError");
const authController = require('../controllers/auth.controller')


exports.passModify = async (req, res, next) => {
    const {vecchiaPassword, password} = req.body;
    const {nome, cognome, mail, data_nascita, citta, telefono, tipologia} = req.body.user;
    if (nome && cognome && mail && password && data_nascita && citta && telefono && (tipologia !== undefined) && vecchiaPassword !== undefined && password !== undefined) {
        try {
            await authController.checkToken(req);
            let encPass = crypto.createHash("sha512").update(password).digest('hex');
            let encVecchiaPass = crypto.createHash("sha512").update(vecchiaPassword).digest('hex');
            let user = await Users.findOne({where: {mail: mail}});
            if (user !== null) {
                if (encVecchiaPass === user.password) {
                    let user = await Users.update({
                        nome: nome,
                        cognome: cognome,
                        mail: mail,
                        password: encPass,
                        data_nascita: data_nascita,
                        citta: citta,
                        telefono: telefono,
                        tipologia: tipologia
                    }, {where: {mail: mail}});
                    if (user[0] === 1) {
                        req.body.user.password = encPass
                        res.send(req.body.user)
                    } else {
                        next(createError(500, "La password corrisponde con quella precedente"))
                    }
                } else {
                    next(createError(403, "La vecchia password non coincide"))
                }
            } else {
                next(createError(404, "utente non trovato nel database"))
            }
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, " una proprietà fra nome , cognome , mail , data_nascita , password , citta, telefono, tipologia not defined"))
    }
}

exports.update = async (req, res, next) => {
    const {nome, cognome, mail, data_nascita, password, citta, telefono, tipologia} = req.body;
    if (nome && cognome && mail && password && data_nascita && citta && telefono && (tipologia !== undefined)) {
        try {
            await authController.checkToken(req);
            //let encPass = crypto.createHash("sha512").update(password).digest('hex');
            let user = await Users.findOne({where: {mail: mail}});
            if (user !== null) {
                let user = await Users.update({
                    nome: nome,
                    cognome: cognome,
                    mail: mail,
                    password: password, //encPass,
                    data_nascita: data_nascita,
                    citta: citta,
                    telefono: telefono,
                    tipologia: tipologia
                }, {where: {mail: mail}});
                if (user[0] === 1) {
                    //req.body.password = encPass
                    res.send(req.body)
                } else {
                    next(createError(400, 'Aggiornamento fallito'));
                }
            } else {
                next(createError(404, 'Utente non trovato'));
            }
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, " una proprietà fra nome , cognome , mail , data_nascita , password , citta, telefono, tipologia not defined"))
    }

}


