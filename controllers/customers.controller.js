const createError = require('http-errors');
const utilsController = require('../controllers/utils.controller')
const authController = require('../controllers/auth.controller')
const AuthError = require("../models/AuthError");

exports.sendDataToPolice = async (req, res, next) => {
    utilsController.uploadSingleFile(req, res, async (err) => {
        if (err){
            next(createError(500, err))
        }
        else{
            try{
                const file = req.file
                const structure = JSON.parse(req.body.structure)
                const user = JSON.parse(req.body.user)

                await authController.checkToken(req);

                let object = {
                    title: "Comunicazione alla questura",
                    nomeS:structure.nome
                }

                Object.assign(object, user);

                await utilsController.sendMail({object, to: 'riki.982009@hotmail.it', pathFile: file.filename, layoutMail:'/mailDataToPolice.ejs' })
                res.send(true)
            }
            catch (e) {
                if (e instanceof AuthError) {
                    next(createError(401, "Unauthorized"));
                } else {
                    next(createError(500,e))
                }
            }

        }
    })
};

exports.sendThreeMonthReport = async (req, res, next) => {
    try{
        let object = req.body
        await authController.checkToken(req);
        object.title = 'Rendiconto trimestrale'
        await utilsController.sendMail({object, to: 'riki.982009@hotmail.it', layoutMail:'/mailThreeMonthReport.ejs' })
        res.send(req.body)
    }
    catch (e){
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(createError(500,e))
        }
    }
};

