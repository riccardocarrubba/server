const createError = require("http-errors");
const db = require("../models");
const Books = db.books;
const Structures = db.structures;
const Users = db.users;
const Rooms = db.rooms;
const AuthError = require("../models/AuthError");
const BookError = require("../models/BookError");
const {sequelize} = require("../models");
const utilsController = require("../controllers/utils.controller");
const authController = require('../controllers/auth.controller')

exports.getAllBooks = async (req, res, next) => {
    try {
        await authController.checkToken(req);
        const [results, metadata] = await sequelize.query(
            `SELECT * from prenotazione`
        );
        res.send(results);
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }
    }
};

exports.getPendingBooksByUser = async (req, res, next) => {
    //getPendingBooksByOwner
    const {id} = req.body;
    if (id !== undefined) {
        try {
            await authController.checkToken(req);
            const [
                books,
                metadata,
            ] = await sequelize.query(`SELECT DISTINCT u.nome as nome_utente,
                                                                                    u.cognome as cognome_utente,
                                                                                    u.mail as mail_utente,
                                                                                    p.*
                                         FROM  utente u,prenotazione p
                                         WHERE p.id_proprietario = ${id} AND
                                                u.id = p.id_cliente AND
                                                p.attesa = true; `);
            res.send(books);
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Id utente relativo alla prenotazione non trovato"));
    }
};

exports.getHistoryBooksByUser = async (req, res, next) => {
    const {id} = req.body;
    if (id !== undefined) {
        try {
            await authController.checkToken(req);
            const now = new Date().toISOString().split("T")[0];
            const [
                books,
                metadata,
            ] = await sequelize.query(`SELECT DISTINCT u.nome as nome_utente,
                                                                                    u.cognome as cognome_utente,
                                                                                    u.mail as mail_utente,
                                                                                    p.*
                                         FROM  utente u, prenotazione p
                                         WHERE  p.id_proprietario = ${id} AND
                                                u.id = p.id_cliente AND
                                                p.attesa = false `);
            res.send(books);
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Id utente relativo alle prenotazioni non trovato"));
    }
};

exports.deletePendingBooksById = async (req, res, next) => {
    const {
        id,
        mail_utente,
        nome_struttura,
        nome_utente,
    } = req.body.prenotazione;
    if (
        id !== undefined &&
        mail_utente !== undefined &&
        nome_struttura !== undefined &&
        nome_utente !== undefined
    ) {
        try {
            await authController.checkToken(req);
            const results = await Books.destroy({where: {id: id}});
            if (results === 1) {
                let object = {
                    title: "Comunicazione importante",
                    nome: nome_utente,
                    body:
                        "Ci dispiace, ma la sua richiesta di prenotazione da lei inviata presso la struttura " +
                        nome_struttura +
                        " è stata rifiutata dal proprietario.",
                };
                await utilsController.sendMail({
                    object,
                    to: mail_utente,
                    layoutMail: "/mailResponsePendingBook.ejs",
                });
                res.send(true);
            } else {
                next(createError(400, "Book not deleted"));
            }
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Id utente relativo alla prenotazione non trovato"));
    }
};
exports.checkBooksById = (id, isStructure, check_in, check_out) => {
    return new Promise(async (resolve, reject) => {
        if (check_in !== null) {
            let existence = [];
            if (isStructure) {
                existence = await sequelize.query(
                    `SELECT * FROM prenotazione p WHERE (p.check_in BETWEEN '${check_in}' AND '${check_out}') AND (p.check_out BETWEEN '${check_in}' AND '${check_out}') AND p.id_camera = ${id} AND p.attesa='false'`,
                    {type: sequelize.QueryTypes.SELECT}
                );
            } else {
                existence = await sequelize.query(
                    `SELECT * FROM prenotazione p WHERE (p.check_in BETWEEN '${check_in}' AND '${check_out}') AND (p.check_out BETWEEN '${check_in}' AND '${check_out}') AND p.id_struttura = ${id} AND p.attesa=false`,
                    {type: sequelize.QueryTypes.SELECT}
                );
            }
            if (existence.length === 0) {
                resolve(true);
            } else {
                reject(
                    new BookError("esiste una prenotazione per la data selezionata")
                );
            }
        } else {
            let existence = [];
            if (isStructure) {
                existence = await sequelize.query(
                    `SELECT * FROM prenotazione p WHERE p.check_out > ${check_out} AND p.id_camera = ${id} AND p.attesa='false'`,
                    {type: sequelize.QueryTypes.SELECT}
                );
            } else {
                existence = await sequelize.query(
                    `SELECT * FROM prenotazione p WHERE p.check_out > ${check_out} AND p.id_struttura = ${id} AND p.attesa=false`,
                    {type: sequelize.QueryTypes.SELECT}
                );
            }
            if (existence.length === 0) {
                resolve(true);
            } else {
                reject(
                    new BookError("esiste una prenotazione ancora in corso")
                );
            }
        }
    });
}

exports.acceptPendingBooks = async (req, res, next) => {
    const {id, mail_utente, nome_struttura, nome_utente, id_camera, id_struttura, check_in, check_out} = req.body.prenotazione;
    if ((id !== undefined) && (mail_utente !== undefined) && (nome_struttura !== undefined) && (nome_utente !== undefined)) {
        try {
            await authController.checkToken(req);
            if (id_camera !== null) {
                await this.checkBooksById(id_camera, true, check_in, check_out);
            } else {
                await this.checkBooksById(id_struttura, false, check_in, check_out);
            }
            let book = await Books.update({
                attesa: false
            }, {where: {id: id}})
            if (book[0] === 1) {
                let object = {
                    title: "Comunicazione importante",
                    nome: nome_utente,
                    body: "La richiesta di prenotazione da lei inviata presso la struttura " + nome_struttura +
                        " Dopo aver eseguito il login come cliente nella nostra piattaforma, puoi pagare al seguente link http://localhost:3000/cartaDiCredito"+
                        " è stata accettata dal proprietario. Essa avrà questo codice di riferimento nella tua area personale: " + id,
                }
                await utilsController.sendMail({object, to: mail_utente, layoutMail: '/mailResponsePendingBook.ejs'})
                res.send(true);
            } else {
                next(createError(400, 'Campi uaguali ai precedenti'));
            }
        } catch (e) {
            if (e instanceof BookError) {
                next(createError(500, e));
            } else if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Campi non inviati correttamente"))
    }
};

exports.createBook = async (req, res, next) => {
    let {
        check_in,
        check_out,
        prezzo,
        id_utente,
        id_camera,
        id_struttura,
        tassa,
        tipologia_ospite,
        richieste,
    } = req.body._data;
    try {
        if (
            check_in !== undefined &&
            richieste !== undefined &&
            check_out !== undefined &&
            prezzo !== undefined &&
            id_utente !== undefined &&
            id_struttura !== undefined &&
            tipologia_ospite !== undefined &&
            tassa !== undefined
        ) {
            await authController.checkToken(req);
            id_camera = id_camera === undefined ? null : id_camera;
            let nome_camera;
            if (id_camera !== null) {
                let camera = await Rooms.findOne({where: {id: id_camera}});
                nome_camera = camera !== null ? camera.nome : null;
            } else nome_camera = null;
            let structure = await Structures.findOne({where: {id: id_struttura}});
            let user = await Users.findOne({where: {id: structure.utente_id}});

            let book = await Books.create({
                check_in: check_in,
                check_out: check_out,
                prezzo: prezzo,
                id_cliente: id_utente,
                id_proprietario: user.id,
                id_camera: id_camera,
                id_struttura: id_struttura,
                nome_camera: nome_camera,
                nome_struttura: structure.nome,
                tassa: tassa,
                tipologia_ospite: tipologia_ospite,
                attesa: true,
            });

            let object = {
                title: "Hai riceavuto una nuova prenotazione",
                nome: user.nome,
                nomeS: structure.nome,
                check_in: check_in,
                check_out: check_out,
                richieste: richieste,
            };
            await utilsController.sendMail({
                object,
                to: user.mail,
                layoutMail: "/mail.ejs",
            });
            res.send(book);
        } else {
            next(
                createError(
                    422,
                    "una proprietà fra check_in, check_out , prezzo, id_utente, id_camera,id_struttura non sono definiti"
                )
            );
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e)
            );
        }
    }
};

exports.allBooksCliente = async (req, res, next) => {
    const id = req.body.prenotazione;
    if (id !== undefined) {
        try {
            await authController.checkToken(req);
            const [
                results,
                metadata,
            ] = await sequelize.query(`SELECT DISTINCT p.id, p.check_in, p.check_out, p.prezzo, p.nome_camera, p.nome_struttura, p.tassa, p.attesa
                                  FROM prenotazione p, utente u
                                  WHERE p.id_cliente = ${id} AND  u.tipologia = 'false'`);
            res.send(results);
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, "Id utente non specificato"));
    }

};

exports.verifyDate = async (req, res, next) => {
    const {check_in, check_out, id_utente, id, tipologia} = req.body.obj;
    var today = new Date();
    var year = today.getFullYear();
    try {
        if (
            check_in !== undefined &&
            check_out !== undefined &&
            id_utente !== undefined &&
            id !== undefined &&
            tipologia !== undefined
        ) {
            if (tipologia === 1) {
                const [
                    response,
                    metadata,
                ] = await sequelize.query(`SELECT sum(datediff(check_out, check_in)) as giorni
                                                        FROM prenotazione p 
                                                        WHERE p.id_camera = ${id} AND p.id_cliente = ${id_utente} 
                                                        AND ( p.check_in BETWEEN '${year}-01-01' AND '${year}-12-31')
                                                        AND ( p.check_out BETWEEN '${year}-01-01' AND '${year}-12-31' )`);
                if (response[0].giorni == null) {
                    response[0].giorni = 0;
                    res.send(response[0]);
                } else {
                    res.send(response[0]);
                }
            } else {
                const [
                    response,
                    metadata,
                ] = await sequelize.query(`SELECT sum(datediff(check_out, check_in)) as giorni
                                                        FROM prenotazione p 
                                                        WHERE p.id_struttura = ${id} AND p.id_cliente = ${id_utente} 
                                                        AND ( p.check_in BETWEEN '${year}-01-01' AND '${year}-12-31')
                                                        AND ( p.check_out BETWEEN '${year}-01-01' AND '${year}-12-31' )`);
                if (response[0].giorni == null) {
                    response[0].giorni = 0;
                    res.send(response[0]);
                } else {
                    res.send(response[0]);
                }
            }
        } else {
            next(
                createError(
                    422,
                    "una proprietà fra check_in, check_out , prezzo, id_utente, id_camera,id_struttura non sono definiti"
                )
            );
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(
                createError(
                    500,
                    e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e
                )
            );
        }
    }
};

exports.verificaGuadagni = async (req, res, next) => {
    const {id} = req.body;
    if (id !== undefined) {
        try {
            await authController.checkToken(req);
            let guadagni = await sequelize.query(
                `SELECT sum(p.prezzo) as guadagni
                                        FROM prenotazione p
                                        WHERE p.id_proprietario= ${id} AND p.attesa='false'`,
                {type: sequelize.QueryTypes.SELECT}
            );
            res.send(guadagni[0].guadagni);
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(createError(422, " l'id del proprietario non è stato trovato "));
    }
};

exports.checkCreditCard = async (req, res, next) => {
        try{
             res.send("La transazione è andata a buon fine.");
            }
        catch (e){
               next(createError(500, e));
            }
    }

