const createError = require("http-errors");
const db = require("../models");
const Structures = db.structures;
const Images = db.images;
const Rooms = db.rooms;
const AuthError = require("../models/AuthError");
const {sequelize} = require("../models");
const utilsController = require("../controllers/utils.controller");
const bookController = require("../controllers/books.controller");
const BookError = require("../models/BookError");
const authController = require('../controllers/auth.controller')

exports.getStructuresByUser = async (req, res, next) => {
    const {id} = req.body;
    if (id !== undefined) {
        try {
            let structures = await Structures.findAll({where: {utente_id: id}});
            if (structures !== null) {
                for await (let structure of structures) {
                    let images = await Images.findAll({
                        where: {id_struttura: structure.id},
                    });
                    structure.setDataValue("paths", images);
                }
                res.send(structures);
            } else {
                next(createError(404, "Non sono state trovate strutture relative al tuo accoun"));
            }
        } catch (e) {
            next(
                createError(
                    500,
                    e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e
                )
            );
        }
    } else {
        next(createError(422, "Utente non trovato"));
    }
};

exports.creaStruttura = async (req, res, next) => {
    const {
        utente_id,
        nome,
        descrizione,
        citta,
        indirizzo,
        tassa,
        cap,
        tipologia,
        telefono,
        wifi,
        parcheggio,
        ascensore_disabili,
        posti_letto,
        prezzo,
    } = req.body;

    try {
        await authController.checkToken(req);
        if (
            (utente_id !== undefined) &&
            (nome !== undefined) &&
            (descrizione !== undefined) &&
            (citta !== undefined) &&
            (indirizzo !== undefined) &&
            (cap !== undefined) &&
            (telefono !== undefined) &&
            (wifi !== undefined) &&
            (parcheggio !== undefined) &&
            (ascensore_disabili !== undefined) &&
            (tipologia !== undefined) &&
            (tassa !== undefined) &&
            (posti_letto !== undefined) &&
            (prezzo !== undefined)
        ) {
            let structure = await Structures.create({
                nome: nome,
                descrizione: descrizione,
                citta: citta,
                tassa: tassa,
                indirizzo: indirizzo,
                cap: cap,
                prezzo: prezzo,
                tipologia: tipologia,
                telefono: telefono,
                wifi: wifi,
                parcheggio: parcheggio,
                ascensore_disabili: ascensore_disabili,
                posti_letto: posti_letto,
                utente_id: utente_id,
            });
            res.send(structure);
        } else {
            next(
                createError(422, "una proprietà fra utente_id, nome, descrizione, citta ,tassa, indirizzo, cap , tipologia, telefono, wifi, parcheggio, ascensore_disabili non sono definiti")
            );
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e)
            );
        }
    }
};

exports.deleteStructureById = async (req, res, next) => {
    const {id} = req.body;
    if (id !== undefined) {
        try {
            await authController.checkToken(req);
            await bookController.checkBooksById(id, false, null, new Date().toISOString().split('T')[0])
            let structureC = await Structures.findOne({where: {id: id}});
            if (structureC !== null) {
                if(structureC.tipologia == 1){
                    let rooms = await Rooms.findAll({where: {id_struttura: id}})
                    if (rooms !== null) {
                        for await (let room of rooms){
                            utilsController.removeImage(room.path)
                        }
                    }
                }
            }
            let structure = await Structures.destroy({where: {id: id}});
            if (structure === 1) {
                res.send(true);
            } else {
                next(createError(400, "Errore eliminazione struttura"));
            }
        } catch (e) {
            if (e instanceof BookError) {
                next(createError(500, e));
            } else if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e)
                );
            }
        }
    } else {
        next(createError(422, "Struttura non trovata"));
    }
};

exports.uploadImage = (req, res, next) => {
    utilsController.upload(req, res, async (err) => {
        const files = req.files;
        const id_struttura = req.body.anotherdata;
        try {
            for (let i = 0; i < files.length; i++) {
                let image = await Images.create({
                    id_struttura: id_struttura,
                    path: files[i].filename,
                });
            }
            res.send({files: files, id_struttura: id_struttura});
        } catch (e) {
            next(
                createError(
                    500,
                    e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e
                )
            );
        }
    });
};

exports.search = async (req, res, next) => {
    const {
        citta,
        posti_letto,
        tipologia,
        prezzo,
        check_in,
        check_out,
        ascensore_disabili,
        parcheggio,
        wifi,
        area_fumatori,
        bagno_disabili,
        animali
    } = req.body;
    try {
        if (
            (citta !== undefined) &&
            (posti_letto !== undefined) &&
            (tipologia !== undefined) &&
            (prezzo !== undefined) &&
            (check_in !== undefined) &&
            (check_out !== undefined) &&
            (ascensore_disabili !== undefined) &&
            (parcheggio !== undefined) &&
            (area_fumatori !== undefined) &&
            (bagno_disabili !== undefined) &&
            (animali !== undefined) &&
            (wifi !== undefined)
        ) {
            let [results, metadata] = [];
            if (tipologia == "1") {
                [results, metadata] = await sequelize.query(`SELECT s.id as s_id, s.nome as s_nome, s.descrizione as s_descrizione, s.citta as s_citta,s.indirizzo as s_indirizzo , s.cap, s.telefono, s.wifi, s.parcheggio,s.ascensore_disabili,s.tassa,s.tipologia , c.*
                                            FROM struttura s , camera c
                                            WHERE c.id_struttura= s.id AND
                                            s.citta LIKE '%${citta}%' AND
                                            c.posti_letto >= ${posti_letto} AND
                                            s.tipologia = ${tipologia} AND
                                            c.prezzo <=  ${prezzo} AND

                                            ${ascensore_disabili === true ? `s.ascensore_disabili = ` + ascensore_disabili + ` AND ` : ``} 
                                            ${parcheggio === true ? `s.parcheggio = ` + parcheggio + ` AND ` : ``} 
                                            ${wifi === true ? `s.wifi = ` + wifi + ` AND ` : ``}

                                            ${area_fumatori === true ? `c.area_fumatori = ` + area_fumatori + ` AND ` : ``} 
                                            ${bagno_disabili === true ? `c.bagno_disabili = ` + bagno_disabili + ` AND ` : ``} 
                                            ${animali === true ? `c.animali = ` + animali + ` AND ` : ``}

                                            c.prezzo = (select min(c.prezzo) from camera c where c.id_struttura= s.id) AND
                                            c.id NOT IN (
                SELECT c2.id
                 FROM prenotazione p, camera c2
                WHERE p.id_camera = c2.id
                AND p.attesa = false AND
                (( p.check_in BETWEEN '${check_in}' AND '${check_out}' )  OR  (p.check_out BETWEEN '${check_in}' AND '${check_out}')) )  group by s.id; `);
                for await (let result of results) {
                    result.s_paths = await Images.findAll({
                        where: {id_struttura: result.s_id},
                    });
                }
            } else if (tipologia == "2") {
                results = await sequelize.query(`SELECT s.*
                                            FROM struttura s WHERE
                                            s.citta LIKE '%${citta}%' AND
                                            s.tipologia = ${tipologia} AND
                                            s.prezzo <= ${prezzo} AND
                                            s.posti_letto >= ${posti_letto} AND
                                            ${ascensore_disabili === true ? `s.ascensore_disabili = ` + ascensore_disabili + ` AND ` : ``} 
                                            ${parcheggio === true ? `s.parcheggio = ` + parcheggio + ` AND ` : ``} 
                                            ${wifi === true ? `s.wifi = ` + wifi + ` AND ` : ``}
                                            s.id NOT IN (
                SELECT p.id_struttura
                 FROM prenotazione p
                   WHERE 
                   p.attesa = false AND (( p.check_in BETWEEN '${check_in}' AND '${check_out}' )  OR  (p.check_out BETWEEN '${check_in}' AND '${check_out}')) )  group by s.id; `,
                    {type: sequelize.QueryTypes.SELECT});
                for await (let result of results) {
                    result.s_paths = await Images.findAll({
                        where: {id_struttura: result.id},
                    });
                }
            }

            res.send(results);
        } else {
            next(createError(422, "campi struttura non inseriti correttamente"));
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(
                createError(
                    500,
                    e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e
                )
            );
        }
    }
};

exports.searchRoomsByStructure = async (req, res, next) => {
    const {posti_letto, id, prezzo, check_in, check_out} = req.body;
    try {
        if (
            posti_letto !== undefined &&
            id !== undefined &&
            prezzo !== undefined &&
            check_in !== undefined &&
            check_out !== undefined
        ) {
            const [
                result,
                metadata,
            ] = await sequelize.query(`SELECT DISTINCT * FROM camera c
                                            WHERE c.id_struttura = ${id} AND
                                             c.posti_letto >= ${posti_letto} AND
                                                c.prezzo <=  ${prezzo} AND
                                            c.id NOT IN (
                SELECT c2.id
                 FROM prenotazione p, camera c2
                WHERE p.id_camera = c2.id AND
                (( p.check_in BETWEEN '${check_in}' AND '${check_out}' )  OR  (p.check_out BETWEEN '${check_in}' AND '${check_out}')) ); `);
            res.send(result);
        } else {
            next(createError(422, "campi struttura non inseriti correttamente"));
        }
    } catch (e) {
        if (e instanceof AuthError) {
            next(createError(401, "Unauthorized"));
        } else {
            next(
                createError(
                    500,
                    e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e
                )
            );
        }
    }
};

exports.updateStructures = async (req, res, next) => {
    const {
        id,
        nome,
        descrizione,
        citta,
        indirizzo,
        tassa,
        cap,
        tipologia,
        telefono,
        wifi,
        parcheggio,
        ascensore_disabili,
        posti_letto,
        prezzo,
        utente_id,
    } = req.body;
    if (
        id !== undefined &&
        nome !== undefined &&
        descrizione !== undefined &&
        utente_id !== undefined &&
        citta !== undefined &&
        indirizzo !== undefined &&
        cap !== undefined &&
        telefono !== undefined &&
        wifi !== undefined &&
        parcheggio !== undefined &&
        ascensore_disabili !== undefined &&
        tipologia !== undefined &&
        tassa !== undefined &&
        posti_letto !== undefined &&
        prezzo !== undefined
    ) {
        try {
            await authController.checkToken(req);
            let structure = await Structures.findOne({where: {id: id}});
            if (structure !== null) {
                let structure = await Structures.update(
                    {
                        nome: nome,
                        descrizione: descrizione,
                        citta: citta,
                        tassa: tassa,
                        indirizzo: indirizzo,
                        cap: cap,
                        prezzo: prezzo,
                        tipologia: tipologia,
                        telefono: telefono,
                        wifi: wifi,
                        parcheggio: parcheggio,
                        ascensore_disabili: ascensore_disabili,
                        posti_letto: posti_letto,
                        utente_id: utente_id,
                    },
                    {where: {id: id}}
                );
                if (structure[0] === 1) {
                    res.send(req.body);
                } else {
                    res.send({updated: false})
                }
            } else {
                next(createError(404, "Struttura non trovata"));
            }
        } catch (e) {
            if (e instanceof AuthError) {
                next(createError(401, "Unauthorized"));
            } else {
                next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
            }
        }
    } else {
        next(
            createError(
                422,
                " una proprietà fra nome , cognome , mail , data_nascita , password , citta, telefono, tipologia not defined"
            )
        );
    }
};

exports.deleteImages = async (req, res, next) => {
    const {id_struttura, path} = req.body.obj;
    console.log(req.body)
    if (id_struttura !== undefined) {
        try {

            let images = await Images.destroy({
                where: {id_struttura: id_struttura},
            });
            for (let i = 0; i < path.length; i++) {
                utilsController.removeImage(path[i].path)
            }
            res.send(true);
        } catch (e) {
            console.log(e)
            next(createError(500, e.original ? (e.original.sqlMessage ? e.original.sqlMessage : e) : e));
        }
    } else {
        next(createError(422, "Struttura non trovata"));
    }
};
