const multer  = require('multer')
const path = require('path')
const nodemailer = require('nodemailer');
const ejs = require('ejs');
const express = require("express");
const fs = require("fs");
const app = express();

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const storageFile = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

const smtpTransport = nodemailer.createTransport({
    service:'Gmail',
    port: 465,
    auth: {
        user:'groupindigo2020@gmail.com',
        pass:"Indigo1234."
    }
});


exports.upload = multer({ storage: storage }).array("images",5 )

exports.uploadSingleFile = multer({ storage: storageFile }).single('file');

exports.removeImage = (path) => {
    try{
        let strpath =  `${__dirname }/../public/images/${path}`
        fs.unlinkSync(strpath);
    }
    catch (e) {
        console.log(e)
    }

}


exports.sendMail =  (parameters) => {
    return new Promise((resolve, reject) => {
        const { object, to, layoutMail, pathFile } = parameters
        ejs.renderFile(app.get("views") + layoutMail, object , function (err, data) {
            if (err) {
                reject(err)
            } else {
                let strpath=`${__dirname }/../public/uploads/${pathFile}`

                let mainOptions;
                if (pathFile !== undefined){
                    mainOptions = {
                        from: '"Indigo 👻 👨‍💻" <groupindigo2020@gmail.com>',
                        to: to,
                        subject: 'Informazioni',
                        attachments: [{
                            path: strpath
                        }],
                        html: data
                    };
                }
                else{
                    mainOptions = {
                        from: '"Indigo 👻 👨‍💻" <groupindigo2020@gmail.com>',
                        to: to,
                        subject: 'Informazioni',
                        html: data
                    };
                }


                smtpTransport.sendMail(mainOptions, function (err, info) {
                    if (err) {
                        reject(err)
                    } else {
                        resolve(true)
                    }
                });
            }
        });
    })

}
