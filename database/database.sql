
CREATE DATABASE IF NOT EXISTS database_progetto;
 USE database_progetto;

 CREATE TABLE utente (
 id bigint NOT NULL AUTO_INCREMENT,
 nome varchar(50) NOT NULL,
 cognome varchar(50) NOT NULL,
 mail varchar(50) NOT NULL,
 password varchar(128) NOT NULL,
 data_nascita DATE NOT NULL,
 tipologia boolean NOT NULL,
 citta varchar(16) NOT NULL,
 telefono varchar(15) NOT NULL,

 unique (mail),
 PRIMARY KEY (id)
 );

 -- password 12345a
 INSERT INTO utente (nome, cognome ,mail, password, data_nascita , tipologia, citta, telefono) VALUES
 ('Riccardo', 'Carrubba','riki.982009@hotmail.it','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', true, 'Palermo','+393801489175'),
 ('Riky', 'Cliente','riky.982009@gmail.com','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', false, 'Palermo','+393801489175'),
 ('Fabio', 'Cognata','fabio.cognata@community.unipa.it','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', true, 'Palermo','+3934124314'),
  ('Fabio', 'Cliente','cognatafabio@gmail.com','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', false, 'Palermo','+3934124314'),
 ('Laura', 'Barbato','laura.barbato01@community.unipa.it','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', true, 'Palermo','+393243242432'),
  ('Laura', 'Cliente','laurabarbato98@icloud.com','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', false, 'Palermo','+393243242432'),
 ('Ishraq', 'Hossain','ishraqhossain.akan@community.unipa.it','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', true, 'Palermo','+3943656757'),
  ('Ishraq', 'Cliente','ishraqhossain2@gmail.com','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', false, 'Palermo','+3943656757'),
 ('Marco Aurelio', 'Macaluso','marcoaurelio.macaluso@community.unipa.it','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', true, 'Palermo','+3934567'),
 ('Marco Aurelio', 'Cliente','macalusomarco98@gmail.com','0a3fd85d00c79751921f1c260b9641ae37655446cc07db3d697362e49fb128b3e1e4d0891554f42e2529c92a937a7ac6c2e13386983431a810e4be775f3ed148', '1998-03-12', false, 'Palermo','+3934567');




 -- -------------------------------------------------------------------------- select * from utente



 CREATE TABLE struttura (
 id bigint NOT NULL AUTO_INCREMENT,
 nome varchar(60) NOT NULL,
 descrizione varchar(250) NOT NULL,
 citta varchar(16) NOT NULL,
 indirizzo varchar(30) NOT NULL,
 cap varchar(5) NOT NULL,
 tipologia int NOT NULL,  -- potremo assegnare un numero per la tipologia della struttura es b & b => 1, casa vacanza => 2 ecc.. (da definire)
 tassa FLOAT NOT NULL,
 telefono varchar(15) NOT NULL,
 wifi boolean NOT NULL,
 parcheggio boolean NOT NULL,
 prezzo int NOT NULL,

 ascensore_disabili boolean NOT NULL,
    posti_letto int NOT NULL,
 utente_id bigint NOT NULL,


 PRIMARY KEY (id),
 FOREIGN KEY (utente_id) REFERENCES utente(id)
 );


 INSERT INTO struttura (nome , descrizione, citta, indirizzo, cap, tipologia,tassa, telefono, wifi, parcheggio, ascensore_disabili,prezzo,posti_letto, utente_id) VALUES
 ('B&b Riccardo', 'La struttura è un b&b che si trova a Roma e ha un plesso moderno, dotata di riscaldamenti e tv', 'Roma', 'Via roma, 10', '90100', 1,12.5, '091 303124231', true, false, true,25, 3, 1),
 ('Casa Vacanza Riccardo', 'La struttura è una casa vacanza che si trova a Venezia, con tv e aria condizionata', 'Venezia', 'Via venezia, 9', '90101', 2, 20, '091 3031', false, false, true,30, 5, 1),
 ('B&b Fabio', 'La struttura è un b&b che si trova a Milano ad un passo dal duomo, con balconcino', 'Milano', 'Via Milano, 8', '90110', 1,12.5, '091 303124231', true, false, true,10, 3, 3),
 ('Casa Vacanza Fabio', 'La struttura è una casa vacanza che si trova a Roma, vicino al Colosseo.', 'Roma', 'Via Del Corso, 150', '90253', 2, 21.5, '091 3031', false, false, true,26,2, 3),
 ('B&b Laura', 'La struttura è un b&b che si trova a Bologna e presso università di Bologna, con aria condizionata', 'Bologna', 'Via Bologna, 101', '90100', 1,22.10, '091 303124231', true, false, true,60,4, 5),
 ('Casa Vacanza Laura', 'La struttura è una casa vacanza che si trova a Palermo, vicino la spiaggio di Mondello, balconcino vista mare incluso', 'Palermo', 'Via Valdesi,50', '90100', 2, 17.5, '091 3031', false, false, true,14,2, 5),
 ('B&b Ishraq', 'La struttura è un b&b che si trova a Firenza, con un prezzo basso offre la qualità per una bella permanenza, compresa anche la tv', 'Fireze', 'Via Firenze, 20', '90100', 1,9.5, '091 303124231', true, false, true,24,6, 7),
 ('Casa Vacanza Ishraq', 'La struttura è una casa vacanza che si trova a Milano e si trova nei pressi della scala di Milano', 'Milano', 'Via del duomo, 90', '90254', 2, 8.5, '091 3031', false, false, true,45,5, 7),
 ('B&b Marco', 'La struttura si trova nel lungomare di palermo, con terrazza vista mare', 'Palermo', 'Via Impreatore Federico, 10', '90100', 1, 7.5, '091 3031', false, false, true,10,2, 9),
 ('Casa Vacanza Marco', 'La struttura è una casa vacanza che si trova a Palermo, nei pressi dei teatri Politeama e Massimo.', 'Palermo', 'Via Ruggero Settimo, 10', '90123', 2, 24, '091 3031', false, false, true,17,3, 9);


 -- --------------------------------------------------------------------------  select * from struttura


 CREATE TABLE camera (
 id bigint NOT NULL AUTO_INCREMENT,
 nome varchar(60) NOT NULL,
 descrizione varchar(250) NOT NULL,
 prezzo int NOT NULL,
 posti_letto int NOT NULL,
 area_fumatori boolean,
 bagno_disabili boolean,
 animali boolean,
 id_struttura bigint NOT NULL,
 path varchar(60) NOT NULL,

 PRIMARY KEY (id),
 FOREIGN KEY (id_struttura) REFERENCES struttura(id) ON UPDATE CASCADE ON DELETE CASCADE
 );


 INSERT INTO camera (nome, descrizione, prezzo, posti_letto, area_fumatori, bagno_disabili, animali,path,id_struttura) VALUES
 ('Camera Riccardo Standard','La stanza comprende pure un balconcino vista mare', 100, 2, true, false, true,'/camera1-riccardo.jpg', 1),
 ('Camera Riccardo Deluxe','La stanza comprende pure un balconcino vista mare', 180, 2, false, false, false,'/camera2-riccardo.jpg', 1),
 ('Camera Fabio Standard','La stanza comprende pure un balconcino vista mare', 60, 2, false, true, true,'/camera1-fabio.jpg', 3),
 ('Camera Fabio Deluxe','La stanza comprende pure un balconcino vista mare', 130, 2, true, false, false,'/camera2-fabio.jpg', 3),
 ('Camera Laura Deluxe','La stanza comprende pure un balconcino vista mare', 160, 2, false, false, false,'/camera1-laura.jpg', 5),
 ('Camera Laura Standard','La stanza comprende pure un balconcino vista mare', 70, 2, true, true, true,'/camera2-laura.jpg', 5),
 ('Camera Ishraq Deluxe','La stanza comprende pure un balconcino vista mare', 120, 2, false, false, false,'/camera1-ishraq.jpg', 7),
 ('Camera Ishraq Standard','La stanza comprende pure un balconcino vista mare', 100, 2, true, false, true,'/camera2-ishraq.jpg', 7),
 ('Camera Marco Standard','La stanza comprende pure un balconcino vista mare', 90, 2, true, false, false,'/camera1-marco.jpg', 9),
 ('Camera Marco Deluxe','La stanza comprende pure un balconcino vista mare', 100, 2, false, true, true,'/camera2-marco.jpg', 9);

 -- --------------------------------------------------------------------------  select * from camera


CREATE TABLE prenotazione (
  id bigint NOT NULL AUTO_INCREMENT,
  check_in DATE NOT NULL,
  check_out DATE NOT NULL,
  prezzo int NOT NULL,
    tipologia_ospite int NOT NULL, -- può essere turista oppure esente da tassa di soggiorno (facciamo intero come tipologia struttura)
  tassa boolean NOT NULL, -- se è false è pagata online, se è true è pagata in struttura

  id_proprietario bigint NOT NULL, -- colui che possiede la struttura
  id_cliente bigint NOT NULL, -- colui che prenota
  id_struttura bigint NOT NULL, -- id della struttura che è stata prenotata
  id_camera bigint,
  nome_camera varchar(30),-- camera prenotata
  nome_struttura varchar(30), -- struttura prenotata
  attesa boolean NOT NULL, -- flag di accettazione del proprietario della camera    false => ancora da accettare ,  true => accettata;

  creata_il TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,

    FOREIGN KEY (id_proprietario) REFERENCES utente(id),
    FOREIGN KEY (id_cliente) REFERENCES utente(id),
  PRIMARY KEY (id)
  );

 INSERT INTO prenotazione(check_in

, check_out , prezzo, id_proprietario,id_cliente, id_struttura, id_camera, nome_camera,nome_struttura, tipologia_ospite,tassa, attesa) VALUES
('2020-09-12', '2020-09-21' ,700, 1,4,1,1, 'Camera Riccardo Standard', 'B&b Riccardo' ,1, false, true ),
('2020-09-19', '2020-09-30' ,800, 1,6,1,2, 'Camera Riccardo Deluxe', 'B&b Riccardo' ,1, false, false ),
('2020-09-12', '2020-09-21' ,560, 1,10,2,null, null, 'Casa Vacanza Riccardo' ,1, false, true ),
('2020-10-10', '2020-10-13' ,230, 3,2,4,null, null, 'Casa Vacanza Fabio' ,2, true, false ),
('2020-10-14', '2020-10-16' ,300, 7,6,7,8, 'B&b Ishraq', 'Camera Ishraq Standard' ,1, true, true ),
('2020-10-20', '2020-10-26' ,505, 9,8,10,null, null, 'Casa Vacanza Marco' ,1, true, true ),
('2020-10-20', '2020-10-25' ,450, 5,10,5,6, 'Camera Laura Standard', 'B&b Laura' ,2, false, false ),
('2020-10-20', '2020-10-25' ,560, 5,8,5,5, 'Camera Laura Deluxe', 'B&b Laura' ,2, false, true ),
('2020-09-12', '2020-09-21' ,800, 5,4,6,null, null, 'Casa Vacanza Laura' ,1, false, true );

 -- --------------------------------------------------------------------------  select * from prenotazione


 CREATE TABLE immagini_struttura(
 id bigint NOT NULL AUTO_INCREMENT,

 id_struttura bigint NOT NULL,
 path varchar(50) NOT NULL,


 PRIMARY KEY (id),
 FOREIGN KEY (id_struttura) REFERENCES struttura(id) ON UPDATE CASCADE ON DELETE CASCADE
 );

 INSERT INTO immagini_struttura(id_struttura, path) VALUES
 (1,'/albero-riccardo-1.jpg'),
  (1,'/albero-riccardo-2.jpg'),
  (2,'/casa-vacanza-riccardo-1.jpg'),
   (2,'/casa-vacanza-riccardo-2.jpg'),
    (2,'/casa-vacanza-riccardo-3.jpg'),
     (2,'/casa-vacanza-riccardo-4.jpg'),
   (3,'/albergo-fabio-1.jpg'),
    (3,'/albergo-fabio-2.jpg'),
     (4,'/casa-vacanza-fabio-1.jpg'),
      (4,'/casa-vacanza-fabio-2.jpg'),
       (4,'/casa-vacanza-fabio-3.jpg'),
     (5,'/albergo-laura-1.jpg'),
     (5,'/albergo-laura-2.jpg'),
      (6,'/casa-vacanza-laura-1.jpg'),
      (6,'/casa-vacanza-laura-2.jpg'),
      (6,'/casa-vacanza-laura-3.jpg'),
      (6,'/casa-vacanza-laura-4.jpg'),
     (7,'/albergo-ishraq-1.jpg'),
      (7,'/albergo-ishraq-2.jpg'),
      (8,'/casa-vacanza-ishraq-1.jpg'),
      (8,'/casa-vacanza-ishraq-2.jpg'),
      (8,'/casa-vacanza-ishraq-3.jpg'),
      (8,'/casa-vacanza-ishraq-4.jpg'),
       (9,'/albergo-marco-1.jpg'),
       (9,'/albergo-marco-2.jpg'),
       (10,'/casa-vacanza-marco-1.jpg'),
        (10,'/casa-vacanza-marco-2.jpg'),
         (10,'/casa-vacanza-marco-3.jpg'),
          (10,'/casa-vacanza-marco-4.jpg');

 -- --------------------------------------------------------------------------  select * from immagini_struttura
